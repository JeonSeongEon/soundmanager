Unity Timeline copyright © 2017-2019 Unity Technologies ApS

Unity 사용 프로젝트를 위한 Unity 컴패니언 라이선스에 따라 사용이 허가 되었습니다. [Unity 컴패니언 라이선스](http://www.unity3d.com/legal/licenses/Unity_Companion_License)를 참조하십시오.

달리 명시적으로 규정하지 않는 한, 이 라이선스에 따른 소프트웨어는 명시적 또는 묵시적 보증 없이 '있는 그대로' 제공됩니다. 이 약관 및 기타 약관에 대한 자세한 내용은 해당 라이선스를 검토하십시오.